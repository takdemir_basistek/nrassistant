#!/bin/bash

# import colors
source ./colors.sh

loading() {
  PID=$!
  i=1
  sp="/-\|"
  echo -n ' '
  while [ -d /proc/$PID ]; do
    printf "\b${sp:i++%${#sp}:1}"
  done
}

OS="Linux"

if [[ "$OSTYPE" == "linux-gnu"* ]]; then
  OS="Linux"
elif [[ "$OSTYPE" == "darwin"* ]]; then
  OS="MacOS"
elif [[ "$OSTYPE" == "cygwin" ]]; then
  OS="windows"
elif [[ "$OSTYPE" == "msys" ]]; then
  OS="windows"
elif [[ "$OSTYPE" == "win32" ]]; then
  OS="windows"
fi

systemName="NR Assistant"
poweredBy="BASISTEK"

echo -e "$FG_GREEN \n ################  WELCOME TO $systemName POWERED BY $poweredBy ################ \n\n $END_COLOR"

#checking user
if [[ $(whoami) != "root" ]]; then
  echo -e "$FG_RED You must be root to start installation $X_CROSS \n $END_COLOR"
  exit
fi

CHECK_DOCKER_SWARM=$(docker info | grep -i -o "Swarm: active")

if [[ $CHECK_DOCKER_SWARM != "Swarm: active" ]]; then
  docker swarm init
  echo -e "$FG_GREEN The Docker swarm is activated $TICK \n $END_COLOR"
else
  echo -e "$FG_GREEN The Docker swarm is active $TICK \n $END_COLOR"
fi

# Checking docker
if [[ $(command -v docker) ]]; then
  echo -e "$FG_GREEN Your $(docker -v) $TICK \n $END_COLOR"
else
  echo -e "$FG_RED Please download The Docker from https://docs.docker.com/engine/install and install it. After then run ./install.sh again $X_CROSS \n $END_COLOR"
  exit
fi

sudo chmod 777 /var/run/docker.sock

# Checking docker compose
if [[ $(command -v docker-compose -v) ]]; then
  echo -e "$FG_GREEN Your $(docker-compose -v). Resuming $systemName installation... \n $END_COLOR"
else
  echo Docker compose does not exist. Install Docker Compose?[y,n]
  read -r dockerInstallPermissionResult
  if [ "$dockerInstallPermissionResult" = n ] || [ "$dockerInstallPermissionResult" = N ]; then
    echo "$FG_RED $systemName installation was terminated $X_CROSS $END_COLOR"
    exit
  else
    curl -L "https://github.com/docker/compose/releases/download/1.29.2/docker-compose-$(uname -s)-$(uname -m)" -o /usr/local/bin/docker-compose
    chmod +x /usr/local/bin/docker-compose
    if [[ ! $(command -v docker-compose -v) ]]; then
      echo "$FG_RED The Docker compose installation has failed. Please install manually and run ./install.sh again $X_CROSS  $END_COLOR"
      exit
    fi
    echo -e "$FG_GREEN The Docker compose was installed successfully. Resuming $systemName installation... $TICK \n $END_COLOR"
  fi
fi

# Checking docker overlay network. If not exist then create
CHECK_DOCKER_NETWORK_IS_EXIST=$(docker network ls | grep -i -o "nr_assistant_network")

if [[ $CHECK_DOCKER_NETWORK_IS_EXIST != "nr_assistant_network" ]]; then
  echo -e "$FG_YELLOW Creating nr_assistant_network. Please wait ... \n $END_COLOR"
  docker network create --driver=overlay --attachable=true nr_assistant_network
  echo -e "$FG_GREEN The Docker new network is created successfully. $TICK \n $END_COLOR"
fi

#Checking whether DB containers are already up or not

compose_up_db_cache_queue_containers() {
  echo -e "$FG_YELLOW Creating database, redis, and cache containers ... \n $END_COLOR"
  # Creating database containers
  echo -e "$FG_GREEN Composing up database, cache and queue containers... \n $END_COLOR"
  if [[ "$OS" == "Linux" || "$OS" == "MacOS" ]]; then
    docker-compose -f docker-compose-db.yaml up -d --build 2>&1 1>/dev/null
  else
    docker-compose -f docker-compose-db.yaml up -d --build >nul 2>nul
  fi
  echo -e "$FG_GREEN Database, cache and queue containers are ready. $TICK \n $END_COLOR"
}

MUST_HAVE_DATABASES=(
  "nr_assistant_rabbitmq"
  "nr_assistant_redis"
  "nr_assistant_mysql")
MUST_HAVE_DATABASES_TOTAL=${#MUST_HAVE_DATABASES[@]}
DB_CACHE_QUEUE_CONTAINERS=$(docker ps --format '{{.Names}}')

if [[ "${DB_CACHE_QUEUE_CONTAINERS}" != "" ]]; then
  containerFound=0
  for container_name in "${MUST_HAVE_DATABASES[@]}"; do
    for container_name_current in $DB_CACHE_QUEUE_CONTAINERS; do
      if [[ "$container_name" == "$container_name_current" ]]; then
        ((containerFound++))
      fi
    done
  done

  if [[ "$MUST_HAVE_DATABASES_TOTAL" == "$containerFound" ]]; then
    echo -e "$FG_GREEN Database, redis, and cache containers already up. Continue... \n $END_COLOR"
  else
    compose_up_db_cache_queue_containers
  fi
elif [[ "${DB_CACHE_QUEUE_CONTAINERS}" == "" ]]; then
  compose_up_db_cache_queue_containers
fi

# Make executable

chmod +x consumer monitoring notification database setup stop-services.sh uninstall.sh restart-services.sh colors.sh

# Copy consumer service
cp ./systemd/nr-consumer.service /etc/systemd/system

# Copy database service
cp ./systemd/nr-database.service /etc/systemd/system

# Copy monitoring service
cp ./systemd/nr-monitoring.service /etc/systemd/system

# Copy notification service
cp ./systemd/nr-notification.service /etc/systemd/system

# Start services
systemctl daemon-reload
systemctl start nr-consumer.service
systemctl enable nr-consumer.service
systemctl start nr-database.service
systemctl enable nr-database.service
systemctl start nr-monitoring.service
systemctl enable nr-monitoring.service
systemctl start nr-notification.service
systemctl enable nr-notification.service

# Getting database credentials

SETUP_TYPE=$(grep -o '"setup_type": "[^"]*' install_credentials.json | grep -o '[^"]*$')

while [[ "$SETUP_TYPE" != "monitoring" ]]; do
  echo -e "$FG_RED Setup type can only be monitoring. Try again. $X_CROSS \n $END_COLOR"
  echo -e "$FG_YELLOW Setup Type [monitoring]: $END_COLOR"
  read -r SETUP_TYPE
done

FIRST_NAME=$(grep -o '"firstname": "[^"]*' install_credentials.json | grep -o '[^"]*$')

while [[ ! $FIRST_NAME =~ ^[A-Za-z]{2,50}$ ]]; do
  echo -e "$FG_RED Firstname is not valid. Min 2, max 50 chars. Try again. $X_CROSS \n $END_COLOR"
  echo -e "$FG_YELLOW Admin Firstname (min 2, max 50 chars): $END_COLOR"
  read -r FIRST_NAME
done

LAST_NAME=$(grep -o '"lastname": "[^"]*' install_credentials.json | grep -o '[^"]*$')

while [[ ! $LAST_NAME =~ ^[A-Za-z]{2,50}$ ]]; do
  echo -e "$FG_RED Lastname is not valid. Min 2, max 50 chars. Try again. $X_CROSS \n $END_COLOR"
  echo -e "$FG_YELLOW Admin Lastname (min 2, max 50 chars): $END_COLOR"
  read -r LAST_NAME
done

EMAIL=$(grep -o '"email": "[^"]*' install_credentials.json | grep -o '[^"]*$')

while [[ ! $EMAIL =~ ^[a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}$ ]]; do
  echo -e "$FG_RED Email is not valid. Try again. $X_CROSS \n $END_COLOR"
  echo -e "$FG_YELLOW Admin email: $END_COLOR"
  read -r EMAIL
done

#PASSWORD=$(grep -o '"password": "[^"]*' install_credentials.json | grep -o '[^"]*$')

echo -e "$FG_YELLOW Admin password (min 8 char, at least 1 uppercase, 1 lowercase, 1 digit, one of special char[*,.,!,#,$,?,@]): $END_COLOR"
prompt=""
PASSWORD=""

while IFS= read -p "$prompt" -r -s -n 1 letter
do
    if [[ $letter == $'\0' ]]
    then
        break
    fi
    PASSWORD+="$letter"
    prompt="*"
done

while ! [[ ${#PASSWORD} -ge 8 && "$PASSWORD" == *[A-Z]* && "$PASSWORD" == *[a-z]* && "$PASSWORD" == *[0-9]* && "$PASSWORD" == *[\*\!\#\$\.\?\@]* ]]; do
  echo -e "$FG_RED Admin password is not valid. Try again. $X_CROSS \n $END_COLOR"
  echo -e "$FG_YELLOW Admin password (min 8 char, at least 1 uppercase, 1 lowercase, 1 digit, one of special char[*,.,!,#,$,?,@]): $END_COLOR"
  unset PASSWORD
  unset prompt
  while IFS= read -p "$prompt" -r -s -n 1 letter
  do
      if [[ $letter == $'\0' ]]
      then
          break
      fi
      PASSWORD+="$letter"
      prompt="*"
  done
done

printf "\n"

TITLE=$(grep -o '"title": "[^"]*' install_credentials.json | grep -o '[^"]*$')

while ! [[ ${#TITLE} -ge 2 && ${#TITLE} -le 100 ]]; do
  echo -e "$FG_RED Title is not valid. Try again. $X_CROSS \n $END_COLOR"
  echo -e "$FG_YELLOW Admin Title (min 2, max 100 chars): $END_COLOR"
  read -r TITLE
done

printf "\n"

MOBILE_PHONE=$(grep -o '"mobile_phone": "[^"]*' install_credentials.json | grep -o '[^"]*$')

while [[ ! $MOBILE_PHONE =~ ^\+?[0-9]{7,25}$ ]]; do
  echo -e "$FG_RED Mobile phone is not valid. Try again. $X_CROSS \n $END_COLOR"
  echo -e "$FG_YELLOW Admin Mobile Phone: $END_COLOR"
  read -r MOBILE_PHONE
done

printf "\n"

#EMAIL_FROM=$(grep -o '"email_from": "[^"]*' install_credentials.json | grep -o '[^"]*$')

echo -e "$FG_YELLOW Email Sender Address (EMAIL_FROM): $END_COLOR"
read -r EMAIL_FROM

while [[ ! $EMAIL_FROM =~ ^[a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}$ ]]; do
  echo -e "$FG_RED Email Sender address is not valid. Try again. $X_CROSS \n $END_COLOR"
  echo -e "$FG_YELLOW Email Sender Address (EMAIL_FROM): $END_COLOR"
  read -r EMAIL_FROM
done

printf "\n"

#SMTP_HOST=$(grep -o '"smtp_host": "[^"]*' install_credentials.json | grep -o '[^"]*$')

echo -e "$FG_YELLOW SMTP Host: $END_COLOR"
read -r SMTP_HOST

while [[ ! $SMTP_HOST =~ ^((25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.){3}(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)$ && ! $SMTP_HOST =~ [A-Za-z0-9-]+(\.[A-Za-z0-9-]+)*(\.[A-Za-z]{2,}) ]]; do
  echo -e "$FG_RED SMTP Host is not valid. Try again. $X_CROSS \n $END_COLOR"
  echo -e "$FG_YELLOW SMTP Host: $END_COLOR"
  read -r SMTP_HOST
done

printf "\n"

#SMTP_PORT=$(grep -o '"smtp_port": "[^"]*' install_credentials.json | grep -o '[^"]*$')
echo -e "$FG_YELLOW SMTP Port: $END_COLOR"
read -r SMTP_PORT
SMTP_PORT="$((SMTP_PORT + 0))"

while [[ ! $SMTP_PORT =~ ^[0-9]{2,5}$ || "$SMTP_PORT" -gt "65535" ]]; do
  echo -e "$FG_RED SMTP port is not valid. Try again. $X_CROSS \n $END_COLOR"
  echo -e "$FG_YELLOW SMTP Port: $END_COLOR"
  read -r SMTP_PORT
done

printf "\n"

#SMTP_PASSWORD=$(grep -o '"smtp_password": "[^"]*' install_credentials.json | grep -o '[^"]*$')
echo -e "$FG_YELLOW SMTP Password: $END_COLOR"

s_prompt=""
SMTP_PASSWORD=""

while IFS= read -p "$s_prompt" -r -s -n 1 letter
do
    if [[ $letter == $'\0' ]]
    then
        break
    fi
    SMTP_PASSWORD+="$letter"
    s_prompt="*"
done

while [[ "$SMTP_PASSWORD" == "" ]]; do
  echo -e "$FG_RED SMTP password is not valid. Try again. $X_CROSS \n $END_COLOR"
  echo -e "$FG_YELLOW SMTP Password: $END_COLOR"
  unset s_prompt
  unset SMTP_PASSWORD
  while IFS= read -p "$s_prompt" -r -s -n 1 letter
  do
      if [[ $letter == $'\0' ]]
      then
          break
      fi
      SMTP_PASSWORD+="$letter"
      s_prompt="*"
  done
done

printf "\n"

echo -e "$FG_YELLOW NR Account ID: $END_COLOR"
read -r NR_ACCOUNT_ID
NR_ACCOUNT_ID="$((NR_ACCOUNT_ID + 0))"

while [[ ! "$NR_ACCOUNT_ID" =~ ^[0-9]{6,8}$ ]]; do
  echo -e "$FG_RED NR account ID is not valid. Try again. $X_CROSS \n $END_COLOR"
  echo -e "$FG_YELLOW NR Account ID: $END_COLOR"
  read -r NR_ACCOUNT_ID
done

printf "\n"

echo -e "$FG_YELLOW NR User Key: $END_COLOR"
nruk_prompt=""
NR_USER_KEY=""

while IFS= read -p "$nruk_prompt" -r -s -n 1 letter
do
    if [[ $letter == $'\0' ]]
    then
        break
    fi
    NR_USER_KEY+="$letter"
    nruk_prompt="*"
done

while [[ "$NR_USER_KEY" == "" ]]; do
  echo -e "$FG_RED NR user key is not valid. Try again. $X_CROSS \n $END_COLOR"
  echo -e "$FG_YELLOW NR User Key: $END_COLOR"
  unset nruk_prompt
  unset NR_USER_KEY

  while IFS= read -p "$nruk_prompt" -r -s -n 1 letter
  do
      if [[ $letter == $'\0' ]]
      then
          break
      fi
      NR_USER_KEY+="$letter"
      nruk_prompt="*"
  done
done

printf "\n"

SLACK_WEBHOOK=$(grep -o '"slack_webhook": "[^"]*' install_credentials.json | grep -o '[^"]*$')

printf "\n"

TEAMS_WEBHOOK=$(grep -o '"teams_webhook": "[^"]*' install_credentials.json | grep -o '[^"]*$')

printf "\n"

# Creating database and seeding them
RESULT=$(./setup "$SETUP_TYPE" "$FIRST_NAME" "$LAST_NAME" "$EMAIL" "$PASSWORD" "$TITLE" "$MOBILE_PHONE" "$EMAIL_FROM" "$SMTP_HOST" "$SMTP_PORT" "$SMTP_PASSWORD" "$NR_ACCOUNT_ID" "$NR_USER_KEY" "$SLACK_WEBHOOK" "$TEAMS_WEBHOOK")

if [[ "$RESULT" != "success" ]]; then
  echo "Error: $RESULT $X_CROSS"
else
  source ./restart-services.sh
  echo -e "$FG_GREEN $systemName is installed successfully. $TICK $END_COLOR"
fi
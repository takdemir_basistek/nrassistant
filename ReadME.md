NR Assistant Installation
-------------
1. Create a folder named nr_assistant under the /opt: <br/>
    `sudo mkdir -p /opt/nr_assistant`
2. Go under nr_assistant file: <br/>
     `cd /opt/nr_assistant`
3. Set sh file as executable as below: <br/>
   `sudo chmod +x install.sh`
4. Execute install.sh as root: <br/>
   `sudo ./install.sh`

NR Assistant Restart Services
-------------
1. Go under nr_assistant file: <br/>
   `cd /opt/nr_assistant`
2. Set sh file as executable as below: <br/>
   `sudo chmod +x restart-services.sh`
3. Run the following command: <br/>
   `sudo ./restart-services.sh` <br/>

NR Assistant Add New Email and CC
-------------
1. Go under nr_assistant file: <br/>
   `cd /opt/nr_assistant`
2. Edit .env file: <br/>
   `vi .env`
3. Add new emails to *_EMAIL environments variable with pipe (|) separator as below:<br/>
    `|Firstname Lastname;firstname.lastname@email.com`
4. Run the following command: <br/>
      `sudo ./restart-services.sh`

NR Assistant Cron
-------------
1. sudo crontab -e
2. `*/5 * * * * /opt/nr_assistant/report_nr_agents_health`
3. `0 17 * * * /opt/nr_assistant/report_currently_open_alert`

NR Assistant Uninstall
-------------
1. Go under nr_assistant file: <br/>
   `cd /opt/nr_assistant`
2. Set sh file as executable as below: <br/>
   `sudo chmod +x uninstall.sh`
3. Run the following command: <br/>
   `sudo ./uninstall.sh` <br/>
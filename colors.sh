#!/bin/bash

export FG_RED="\033[0;31m"
#export BG_RED="\033[0;41m"
export FG_GREEN="\033[0;32m"
#BG_GREEN="\033[0;42m"
export FG_YELLOW="\033[0;33m"
#BG_YELLOW="\033[0;43m"
export END_COLOR="\033[0m"
export TICK="\u2714"
export X_CROSS="\u274c"
#!/bin/bash

source ./stop-services.sh

sudo rm -rf /etc/systemd/system/nr-database.service rm -rf /etc/systemd/system/nr-notification.service rm -rf /etc/systemd/system/nr-monitoring.service rm -rf /etc/systemd/system/nr-consumer.service

sudo docker-compose -f docker-compose-db.yaml down
sudo docker network rm nr_assistant_network